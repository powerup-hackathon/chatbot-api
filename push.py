import os
import sys
import shutil
import zipfile
import argparse

from src.env import config
from shutil import move
from os.path import join

def zip(osType):
    print('ZIPPING THE PACKAGES')
    zipName = 'zf.zip'
    if(os.path.exists(zipName)):
        os.remove(zipName)
        print("deleted " + zipName)

    if(osType=='windows'):
        command = '7z a ' + zipName + ' * ./package/* -mx0 -xr!__pycache__ -xr!.git -xr!.vscode -xr!package -xr!settings.*.conf -xr!_venv'
        os.system(command)

    elif(osType=='linux'):
        print("zipping new File in linux")
        packageLoc = os.getcwd() + "/package/"
        gitLoc = os.getcwd() + "/.git/"
        ignoreFiles = [
            "zf.zip",
            "settings.dev.py",
            "settings.prod.py"
        ]
        # print(packageLoc)
        zipf = zipfile.ZipFile('zf.zip', 'w', zipfile.ZIP_DEFLATED)
        for root, dirs, files in os.walk(os.getcwd()):
            # print(root)
            # print(dirs)
            # print(files)
            for filename in files:
                # print(os.path.join(root, filename).replace(os.getcwd()+"/",""))
                # print(os.path.join(root, filename))
                if (filename in ignoreFiles):
                    pass
                elif gitLoc in root:
                    pass
                elif packageLoc in root:
                    fileToZip = os.path.join(root, filename)
                    LocOfZip = os.path.join(root, filename).replace(os.getcwd()+"/package/","/")
                    zipf.write(fileToZip,LocOfZip)
                else:
                    fileToZip = os.path.join(root, filename)
                    LocOfZip = os.path.join(root, filename).replace(os.getcwd()+"/","")
                    zipf.write(fileToZip,LocOfZip)
        zipf.close()

# def install_requirements(osType):
#     print('INSTALLING REQUIREMENTS')
#     if(os.path.exists('requirements.txt')):
#         if(osType=='windows'):
#             command = 'pip wheel --wheel-dir=/local/wheels -r requirements.txt'
#             command = 'pip install --no-index --find-links=/local/wheels -r requirements.txt -t ./package'
#         elif(osType=='linux'):
#             pass
#         os.system(command)


def push(functionName,region=None,s3Upload=False):
	if(s3Upload):
		print()
		print('UPLOADING TO ' + config.get_setting('aws')['s3']['bot'])
		os.system('aws s3 cp zf.zip s3://'+ config.get_setting('aws')['s3']['bot'] + '/LAMBDA/')
		print()
		print('UPDATING LAMBDA ' + functionName)
		os.system('aws lambda update-function-code --function-name ' + functionName + ' --s3-bucket '+ config.get_setting('aws')['s3']['bot'] +' --s3-key LAMBDA/zf.zip')
	else:
		print('UPDATING LAMBDA ' + functionName)
		os.system('aws lambda update-function-code --function-name '+functionName+' --zip-file fileb://zf.zip')

parser = argparse.ArgumentParser(description='PUSH TO LAMBDA')
parser.add_argument('-e','--env',type=str,help='Indicate to which environment will the lambda be pushed: dev / prod')
parser.add_argument('-o','--os',type=str,help='Indicate OS type: windows / linux')
args = parser.parse_args()

if(args.os and args.env):
    if(args.env.lower() in ['dev','prod','test']):
        if(args.os.lower() in ['windows','linux']):
            fileName = 'src/env/settings.' + args.env.lower() + '.conf'
            copyFileName = 'src/env/settings.conf'
            if(os.path.exists(copyFileName)):
                os.remove(copyFileName)
            shutil.copy(fileName,copyFileName)

            toPush = False

            # INSTALL REQUIREMENTS
            # install_requirements(args.os.lower())

            # ZIP FOLDER
            zip(args.os.lower())

            lambdaName = config.get_setting('aws')['lambda']['bot']
            print('You are going to push on ' + lambdaName + '...')
            print('Confirm (y/n):', end='')
            ans = input()
            if(ans=='y'):
                toPush = True
            else:
                print('canceled changes')

            if(toPush):
                push(lambdaName) #ENABLE IF TO PUSH ALREADY
                # pass
        else:
            parser.error("OS must be 'windows' or 'linux' only")
    else:
        parser.error("Environment must be 'dev' or 'prod' only")
else:
	parser.error('Indicate os type (windows/linux) and if upload to s3')