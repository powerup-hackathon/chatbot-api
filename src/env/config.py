import json

def get_setting(key):
    """
    Returns value of key in setting.conf
    """
    try:
        keyValue = None
        with open('src/env/settings.conf') as configFile:
            fileContent = json.loads(configFile.read())
            keyValue = fileContent[key]
            configFile.close()
    except Exception as e:
        print('CONFIG ERROR         : ' + str(e) )
    finally:
        return keyValue

def get_env_variables():
    """
    Returns value of key from AWS Lambda
    """
    import boto3
    awsRegion = get_setting('aws')['region']
    lambdaName = get_setting('aws')['lambda']['bot']
    lambdaFunction = boto3.client('lambda',region_name=awsRegion)
    lambdaConfig = lambdaFunction.get_function_configuration(FunctionName=lambdaName)
    lambdaKeys = lambdaConfig['Environment']['Variables']
    return lambdaKeys

def get_key(key):
    envVariables = get_env_variables()
    return envVariables[key]