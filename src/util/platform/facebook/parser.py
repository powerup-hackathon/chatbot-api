class Parser:
    """
    ASSUMPTION: SINGLE USER PER REQUEST
    """
    def __init__(self,event):
        messagingPayload = event['entry'][0]['messaging'][0]
        self._objectType = event['object']
        self._userID = messagingPayload['sender']['id']
        self._communityID = messagingPayload['sender']['id']
        self._timestamp = messagingPayload['timestamp']
        self._messagingPayload = messagingPayload

    def __del__(self):
        pass

    def __str__(self):
        return ( 'OBJECT TYPE          : %s\n' \
               + 'USER ID              : %s\n'\
               + 'COMMUNITY ID         : %s\n'\
               + 'TIMESTAMP            : %s\n')\
               % (self.objectType,self.userID,self.communityID,self.timestamp)

    @property
    def objectType(self):
        return self._objectType

    @property
    def userID(self):
        return self._userID

    @property
    def communityID(self):
        return self._communityID

    @property
    def timestamp(self):
        return self._timestamp

    @property
    def messagingPayload(self):
        return self._messagingPayload

    @objectType.setter
    def objectType(self,objectType):
        self._objectType = objectType

    @userID.setter
    def userID(self,userID):
        self._userID = userID

    @communityID.setter
    def communityID(self,communityID):
        self._communityID = communityID

    @timestamp.setter
    def timestamp(self,timestamp):
        self._timestamp = timestamp

    @messagingPayload.setter
    def messagingPayload(self,messagingPayload):
        self._messagingPayload = messagingPayload


    def parse(self):
        """
        IF MULTIPLE ATTACHMENTS WERE SENT:
            - IF ALL ATTACHMENT HAS THE SAME TYPE, THEY WILL BE PART OF ONE PAYLOAD
            - IF AN ATTACHMENT IS DIFFERENT FROM THE OTHER ATTACHMENTS, THEY WILL HAVE A SEPARATE PAYLOAD.

        IF ATTACHMENTS WITH TEXT
            - TEXT WILL HAVE A SEPARATE PAYLOAD
        """
        tempInputType = None
        tempData = {}

        data = None
        if('message' in self.messagingPayload):
            message = self.messagingPayload['message']

            # QUICK REPLY
            if('quick_reply' in message):
                tempInputType = 'quick_reply'
                tempData['payload'] = message['quick_reply']['payload']
                tempData['text'] = message['text']
                data = self.set_data_payload(tempInputType,tempData)

            # ATTACHMENT
            elif('attachments' in message):
                data = []
                attachments = message['attachments']
                for attachment in attachments:
                    tempInputType = attachment['type']
                    tempData['payload'] = attachment['payload']['url']
                    data.append(self.set_data_payload(tempInputType,tempData))

                newData = {
                    'items': data
                }
                data = self.set_data_payload('attachment',newData)

            else:
                tempInputType = 'text'
                tempData['text'] = message['text']
                data = self.set_data_payload(tempInputType,tempData)

        elif('postback' in self.messagingPayload):
            postback = self.messagingPayload['postback']

            tempInputType = 'button'
            tempData['payload'] = postback['payload']
            tempData['text'] = postback['title']

            data = self.set_data_payload(tempInputType,tempData)

        return data

    @staticmethod
    def set_data_payload(inputType,data):
        return {
            'type': inputType,
            'data': data
        }