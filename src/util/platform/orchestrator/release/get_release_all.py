from src.util.api import api
def exe(token,accountLogicalName,serviceInstanceLogicalName):
    """
    Get's access token of account and token id using refresh token.
    """
    method = 'get'
    url = 'https://platform.uipath.com/%s/%s/odata/Releases' %(accountLogicalName,serviceInstanceLogicalName)
    params = {}
    headers = {
        'Authorization':'Bearer ' + token,
        'Content-Type': 'application/json',
        'X-UIPATH-TenantName': serviceInstanceLogicalName
        }
    data = {}
    return api.execute_call(method,url,headers,data,params,True)

