from src.util.api import api
def exe(token,accountLogicalName,serviceLogicalName,payload):
    """
    Get's access token of account and token id using refresh token.
    """
    method = 'post'
    url = 'https://platform.uipath.com/%s/%s/odata/Jobs/UiPath.Server.Configuration.OData.StartJobs' % (accountLogicalName,serviceLogicalName)

    params = {}
    headers = {
        'Authorization':'Bearer ' + token,
        'Content-Type': 'application/json',
        'X-UIPATH-TenantName': serviceLogicalName
    }
    data = payload
    return api.execute_call(method,url,headers,data,params,True)

