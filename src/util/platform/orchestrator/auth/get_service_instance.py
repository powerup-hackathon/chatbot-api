from src.util.api import api
def exe(token,accountLogicalName):
    """
    Get's access token of account and token id using refresh token.
    """
    method = 'get'
    url = 'https://platform.uipath.com/cloudrpa/api/account/'+accountLogicalName+'/getAllServiceInstances'
    params = {}
    headers = {'Authorization':'Bearer ' + token}
    data = {}
    return api.execute_call(method,url,headers,data,params,True)

