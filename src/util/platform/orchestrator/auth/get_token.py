from src.util.api import api
def exe(token):
    """
    Get's access token of account and token id using refresh token.
    """
    method = 'post'
    url = 'https://account.uipath.com/oauth/token'
    params = {}
    headers = {'Content-Type':'application/json'}
    data = {
        'grant_type':'refresh_token',
        'client_id':'5v7PmPJL6FOGu6RB8I1Y4adLBhIwovQN',
        'refresh_token': token
    }
    return api.execute_call(method,url,headers,data,params,True)

