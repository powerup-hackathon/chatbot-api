import boto3
from src.env import config
regionName = config.get_setting('aws')['region']
def init_resource(table):
    return boto3.resource('dynamodb',region_name=regionName).Table(table)

def init_client():
    return boto3.client('dynamodb',region_name=regionName)

def set_regionName(rn):
    global regionName
    regionName = rn

def update_item(tableName,key,item):
    """
    NOTE: COULD NOT UPDATE SPECIFIC ITEM IN MAP. UPDATES THE WHOLE MAP WHICH DELETES CURRENT MAP KEY-VALUE PAIRS
    Key = { <keyname>: { <attributeType>: <value>} }
    AttributeUpdates = { <keyName>: { 'Value': { <attributeType>: <value> } } }
    Attribute Types: S | M | BOOL | N | B
    """
    dbClient = init_client()
    return dbClient.update_item(
        TableName = tableName,
        Key = key,
        AttributeUpdates = item,
        ReturnValues='UPDATED_NEW'
    )

def update_map(tableName,key,updateExpression,expressionAttributeNames,expressionAttributeValues):
    """
    CAN ALSO ACT AS AN UPDATE ITEM
    response = table.update_item(
        Key={
            'param': 'last'
        },
        UpdateExpression='SET #VALUE.#FIELD = :value',
        ExpressionAttributeNames={
            '#VALUE': 'value',
            '#FIELD': 'field2',
        },
        ExpressionAttributeValues={
            ':value': 422
        },
        ReturnValues='UPDATED_NEW'
    )
    """
    dbResource = init_resource(tableName)
    return dbResource.update_item(
        Key=key,
        UpdateExpression=updateExpression,
        ExpressionAttributeNames=expressionAttributeNames,
        ExpressionAttributeValues=expressionAttributeValues,
        ReturnValues='UPDATED_NEW'
    )

def delete_item(tableName,key):
    """
    key = { <keyName> : { <attributeType>: <value> } }
    Attribute Types: S | M | BOOL | N | B
    """
    dbClient = init_client()
    return dbClient.delete_item(
        TableName=tableName,
        Key=key
    )

def query_item(tableName,key,**kwargs):
    """
    Use from boto3.dynamodb.conditions import Key,Attr for 'Key'
    Key = Key(<keyName>).eq(<value>)
    """
    dbResource = init_resource(tableName)
    return dbResource.query(
        KeyConditionExpression=key,
        **kwargs
    )


def scan_table(tableName,responseData=[],attr=None,lastKey=None,):
    params = {}
    if(lastKey):
        params['ExclusiveStartKey'] = lastKey

    if(attr):
        params['FilterExpression'] = attr

    dbResource = init_resource(tableName)
    scanResponse = dbResource.scan(**params)

    responseData = scanResponse['Items']
    if('LastEvaluatedKey' in scanResponse):
        return scan_table(tableName,responseData,attr,scanResponse['LastEvaluatedKey'])
    return responseData


