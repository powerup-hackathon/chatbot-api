import json
import requests


def set_url_endpoint(url,endpoint=None):
    return not endpoint and url or url + '/' + endpoint

def set_return(response):
    print(response.content)
    print()
    if(response.status_code==200):
        return response.json(),response.status_code
    return {},response.status_code

def execute_call(method,url,headers,data=None,params=None,isJson=False):
    """
    Sends request to api endpoint.
    """
    # print(method)
    # print(url)
    # print(headers)
    # print(data)
    # print(params)
    response = None
    if(not isJson):
        if(method=='post'):
            if(params):
                return set_return(requests.post(url,data=data,headers=headers,params=params))

            return set_return(requests.post(url,data=data,headers=headers))

        elif(method=='get'):
            if(params):
                if(data):
                    return set_return(requests.get(url,headers=headers,params=params,data=data))

                return set_return(requests.post(url,data=data,headers=headers))

            if(data):
                return set_return(requests.get(url,headers=headers,json=data))
    else:
        if(method=='post'):
            if(params):
                return set_return(requests.post(url,json=data,headers=headers,params=params))
            return set_return(requests.post(url,json=data,headers=headers))

        elif(method=='get'):
            if(params):
                if(data):
                    return set_return(requests.get(url,headers=headers,params=params,json=data))
                return set_return(requests.post(url,json=data,headers=headers))

            if(data):
                return set_return(requests.get(url,headers=headers,json=data))
            return set_return(requests.get(url,headers=headers))
    return {}, 400
