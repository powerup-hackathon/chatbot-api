def parse(jsondict,n):
	print('{')
	n+=1
	for key in jsondict:
		tab = ''
		for i in range(0,n):
			print('>>>',end='')

		print('['+key+']',end='')
		print(' : ',end='')
		value = jsondict[key]
		if("dict" in str(type(value))):
			n+=1
			parse(value,n)
		elif("list" in str(type(value))):
			for item in value:
				if("dict" in str(type(item))):
					n+=1
					parse(item,n)
				else:
					print(item,end='\n')
		else:
			print(jsondict[key],end='\n')
	tab = ''
	for i in range(0,n):
		print('>>>',end='')
	print('}')
