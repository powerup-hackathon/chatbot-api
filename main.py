from src.util.platform.facebook.parser import Parser
from src.util.platform.orchestrator import get_token,get_accounts_for_user,get_service_instance

from src.util.platform.orchestrator import add_item

from src.util.platform.orchestrator import start_job

from src.util.platform.orchestrator import get_all_robots

from src.util.platform.orchestrator import get_release_by_name,get_release_all

import requests
import json

def lambda_handler(event,context):
  print('EVENT                : ' + str(event))

  eventObject = Parser(event)
  # print(eventObject)
  userID = eventObject.userID
  requestData = str(eventObject.parse())
  print(userID)
  print(requestData)

  # AUTH
  accessToken,accountLogicalName,serviceLogicalName = orchestrator_authenticate()
  # print(accessToken)

  # GET ROBOT
  # response,status = get_all_robots.exe(accessToken,accountLogicalName,serviceLogicalName)
  # print(response)

  # ADD ITEM
  orchestrator_addItem(userID,requestData,accessToken,accountLogicalName,serviceLogicalName)

  # START JOB
  orchestrator_startJob(accessToken,accountLogicalName,serviceLogicalName)

  # # GET RELEASE BY NAME
  # orchestrator_get_release_by_name(accessToken,accountLogicalName,serviceLogicalName)

  # # GET RELEASES ALL
  # orchestrator_get_release_all(accessToken,accountLogicalName,serviceLogicalName)

  return 200



def orchestrator_authenticate():
  refreshToken = None
  with open('./src/assets/orchestrator_refresh_token.txt', 'r') as fb:
    refreshToken = fb.read()
    fb.close()

  tokenResponse,tokenStatusCode = get_token.exe(refreshToken)
  idToken = tokenResponse['id_token']
  accessToken = tokenResponse['access_token']

  accountsResponse,accountsStatusCode = get_accounts_for_user.exe(idToken)
  accountLogicalName = accountsResponse['accounts'][0]['accountLogicalName']

  serviceResponse,serviceStatusCode = get_service_instance.exe(idToken,accountLogicalName)
  serviceLogicalName = serviceResponse[0]['serviceInstanceLogicalName']

  return accessToken,accountLogicalName,serviceLogicalName


def orchestrator_addItem(userID,requestData,accessToken,accountLogicalName,serviceLogicalName):

  queueName = 'ChatbotQueue'
  payload = {
    'itemData': {
      'Name': queueName,
      'Priority': 'Normal',
      'SpecificContent': {
        'UserID': userID,
        'RequestData': str(requestData),
        'RequestData@odata.type': '#String'
      }
    }
  }
  response,code = add_item.exe(accessToken,accountLogicalName,serviceLogicalName,payload)
  print('ADD ITEM CODE: ' + str(code))
  print('ADD ITEM RESPONSE: ' + str(response))
  return response

def orchestrator_startJob(accessToken,accountLogicalName,serviceLogicalName):
  releaseKey = '871105ed-27e6-45a2-9ce5-a3f7d66d1652'
  robotIds = [199526]
  jobsCount = 0
  payload = {
    'startInfo': {
      'ReleaseKey': releaseKey,
      'Strategy': 'Specific',
      'RobotIds': robotIds,
      'JobsCount': jobsCount,
      'Source': 'Manual'
    }
  }
  response,code = start_job.exe(accessToken,accountLogicalName,serviceLogicalName,payload)
  print('ADD ITEM CODE: ' + str(code))
  print('ADD ITEM RESPONSE: ' + str(response))
  return response

def orchestrator_get_release_by_name(accessToken,accountLogicalName,serviceLogicalName):
  processName = 'rpa-chatbot-framework'
  response,code = get_release_by_name.exe(accessToken,accountLogicalName,serviceLogicalName,processName)
  return response

def orchestrator_get_release_all(accessToken,accountLogicalName,serviceLogicalName):
  response,code = get_release_all.exe(accessToken,accountLogicalName,serviceLogicalName)
  return response
