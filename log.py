import sys
import boto3
import json_parse
from src.env import config

global logs

# ---- INITIALIZES CLOUDWATCH LOGS ---
def init():
	global logs
	logs = boto3.client(
		'logs'
	)

def get_stream_name(groupName):
	global logs
	init()
	resp = logs.describe_log_streams(
		logGroupName=groupName,
		orderBy='LastEventTime',
		descending=True,
		limit = 1
	)
	return resp['logStreams'][0]['logStreamName']


def get_log_stream(groupName,limit):
	global logs

	init()

	streamName = get_stream_name(groupName)

	resp = logs.get_log_events(
		logGroupName = groupName,
		logStreamName = streamName,
	)

	return resp['events']


if __name__ == '__main__':
	if(len(sys.argv)>0):
		command = config.get_setting('aws')['lambda']['bot']
	else:
		command = sys.argv[1]
	streams = get_log_stream('/aws/lambda/'+command,0)
	for stream in streams:
		print(stream['message']+'\n')
